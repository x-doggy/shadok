#ifndef SHADOK_POINT_H
#define SHADOK_POINT_H

struct tag_point_t {
  int x, y;
};

typedef struct tag_point_t point_t;

void point_plus(point_t *dest, const point_t *a, const point_t *b);
void point_minus(point_t *dest, const point_t *a, const point_t *b);

#endif