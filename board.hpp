#ifndef _SHADOK_BOARD_H_
#define _SHADOK_BOARD_H_

#include <cstdio>

#define BOARD_SIZE_HOR 20
#define BOARD_SIZE_VER 12

struct tag_board_t {
  char * board[BOARD_SIZE_VER];
};

typedef struct tag_board_t board_t;

void board_print(const board_t *board);
void board_clear(board_t *board, char clearSym);

#endif
