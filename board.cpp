#include "board.hpp"

void board_print(const board_t *board) {
  for (int i = 0; i < BOARD_SIZE_VER; i++) {
    printf("%s\n", board->board[i]);
  }
  printf("\n");
}

void board_clear(board_t *board, char clearSym) {
  for (int i = 0; i < BOARD_SIZE_VER; i++) {
    for (int j = 0; j < BOARD_SIZE_HOR; j++) {
      board->board[i][j] = clearSym;
    }
  }
}
