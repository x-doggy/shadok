#include "game.hpp"

point_t DIRECTION_UP         = {-1,  0};
point_t DIRECTION_DOWN       = { 1,  0};
point_t DIRECTION_LEFT       = { 0, -1};
point_t DIRECTION_RIGHT      = { 0,  1};
point_t DIRECTION_UP_LEFT    = {-1,  1};
point_t DIRECTION_UP_RIGHT   = {-1,  1};
point_t DIRECTION_DOWN_LEFT  = { 1, -1};
point_t DIRECTION_DOWN_RIGHT = { 1,  1};

int is_cell_empty(const board_t *board, const point_t *p) {
  return board->board[p->x][p->y] == EMPTY_SYMB;
}

int is_cell_giby(const board_t *board, const point_t *p) {
  return board->board[p->x][p->y] == GIBY_SYMB;
}

int is_cell_shadok(const board_t *board, const point_t *p) {
  return board->board[p->x][p->y] == SHADOK_SYMB;
}

int is_cell_flower(const board_t *board, const point_t *p) {
  return NULL != (char *) memchr("123456789", board->board[p->x][p->y], 9);
}

int is_out_of_border_bounds(const point_t *p) {
  return p->x >= BOARD_SIZE_VER || p->y >= BOARD_SIZE_HOR;
}

void generate_random_point(point_t *p) {
  p->x = rand_range(0, BOARD_SIZE_VER);
  p->y = rand_range(0, BOARD_SIZE_HOR);
}

int place_player_to_empty_position_on_board(
    board_t *board,
    const point_t *p,
    const char player
) {
#ifdef _SHADOK_DEBUG_MESSAGES
  printf("PLACE PLAYER TO BOARD: (%d, %d);\n", p->x, p->y);
#endif
  assert(p != NULL && p->x < BOARD_SIZE_VER && p->y < BOARD_SIZE_HOR);
  
  if (is_cell_empty(board, p)) {
    board->board[p->x][p->y] = player;
    return C_PLAYER_PLACED_SUCCESSFUL;
  }
  return C_PLAYER_ALREADY_PLACED;
}

int place_player_to_board_by_random(
    board_t *board,
    const char player,
    point_t *newPoint
) {
  int err;
  do {
    generate_random_point(newPoint);
    err = place_player_to_empty_position_on_board(board, newPoint, player);
  } while (err != C_PLAYER_PLACED_SUCCESSFUL);
  return err;
}

point_t process_direction(char d[]) {
  // d -> to lower case
  transform_to_lower_case(d);
  
  point_t newPosition;
  newPosition.x = 0;
  newPosition.y = 0;

  if ( ! strcmp(d, "u") || ! strcmp(d, "uu") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("UP"));
#endif
    newPosition.x = DIRECTION_UP.x;
    newPosition.y = DIRECTION_UP.y;
  } else if ( ! strcmp(d, "l") || ! strcmp(d, "ll") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("LEFT"));
#endif
    newPosition.x = DIRECTION_LEFT.x;
    newPosition.y = DIRECTION_LEFT.y;
  } else if ( ! strcmp(d, "r") || ! strcmp(d, "rr") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("RIGHT"));
#endif
    newPosition.x = DIRECTION_RIGHT.x;
    newPosition.y = DIRECTION_RIGHT.y;
  } else if ( ! strcmp(d, "d") || ! strcmp(d, "dd") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("DOWN"));
#endif
    newPosition.x = DIRECTION_DOWN.x;
    newPosition.y = DIRECTION_DOWN.y;
  } else if ( ! strcmp(d, "ul") || ! strcmp(d, "lu") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("UP LEFT"));
#endif
    newPosition.x = DIRECTION_UP_LEFT.x;
    newPosition.y = DIRECTION_UP_LEFT.y;
  } else if ( ! strcmp(d, "ur") || ! strcmp(d, "ru") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("UP RIGHT"));
#endif
    newPosition.x = DIRECTION_UP_RIGHT.x;
    newPosition.y = DIRECTION_UP_RIGHT.y;
  } else if ( ! strcmp(d, "dl") || ! strcmp(d, "ld") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("DOWN LEFT"));
#endif
    newPosition.x = DIRECTION_DOWN_LEFT.x;
    newPosition.y = DIRECTION_DOWN_LEFT.y;
  } else if ( ! strcmp(d, "dr") || ! strcmp(d, "rd") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("DOWN RIGHT"));
#endif
    newPosition.x = DIRECTION_DOWN_RIGHT.x;
    newPosition.y = DIRECTION_DOWN_RIGHT.y;
  } else if ( ! strcmp(d, "") ) {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[%12s] direction\n", _("HERE"));
#endif
  }
  
  return newPosition;
}

void move_player(
    board_t *board,
    const point_t *newPosition,
    point_t *playerP,
    const char player,
    point_t **flowers
) {
  if (is_cell_flower(board, newPosition)) {
    if (player == SHADOK_SYMB) {
      extern int score;
      score += board->board[newPosition->x][newPosition->y] - '0';
    }
    generate_new_flower(board, newPosition, flowers);
  } else if (is_cell_giby(board, newPosition)) {
    return;
  }
  board->board[playerP->x][playerP->y] = EMPTY_SYMB;
  playerP->x = newPosition->x;
  playerP->y = newPosition->y;
  board->board[playerP->x][playerP->y] = player;
}

void generate_new_flower(board_t *board, const point_t *newPosition, point_t **flowers) {
  int i = 0;
  while (i < FLOWERS_NUMBER &&
        flowers[i]->x == newPosition->x &&
        flowers[i]->y == newPosition->y) {
    i++;
  }
  /* Now i = {index of destroyed flower} */
  if (flowers[i] != NULL) free(flowers[i]);
  place_player_to_board_by_random(
      board,
      (const char) (rand_range(1, 9) + '0'),
      flowers[i]
  );
}

void process_new_position(
    board_t *board,
    point_t *direction,
    point_t *playerP,
    const char player,
    point_t **flowers
) {
  point_t newPosition;
  newPosition.x = playerP->x + direction->x;
  newPosition.y = playerP->y + direction->y;
  
  if (!is_out_of_border_bounds(&newPosition)) {
    move_player(board, &newPosition, playerP, player, flowers);
  } else {
#ifdef _SHADOK_DEBUG_MESSAGES
    printf("OUT OF BOUNDS '%c': (%d, %d);\n", player, newPosition.x, newPosition.y);
#endif
  }
}

void process_enemies(board_t *board, point_t **gibies, point_t **flowers) {
  for (int i = 0; i < GIBIES_NUMBER; i++) {
    /* Find a nearest flower around every giby. */
    /* And do one step to the way of flower. */
    const point_t *g = gibies[i];
    /* Actual infinity */
    int min = 600, minX = BOARD_SIZE_HOR + 1, minY = BOARD_SIZE_VER + 1;
    for (int j = 0; j < FLOWERS_NUMBER; j++) {
      const point_t *f = flowers[j];
      const int distance = hypot_squared(f->x, g->x);
      if (distance < min) {
        min = distance;
        minX = f->x;
        minY = f->y;
      }
    }
    
    if (min == 1 || min == 2) {
      point_t minPoint = {minX, minY};
      move_player(board, &minPoint, gibies[i], GIBY_SYMB, flowers);
      continue;
    }
    
    /* Giby moves to flower in the direction of min */
    point_t direction = {0, 0};
    if (g->x == minX) {
      if (g->y < minY) {
        /* to UP */
        direction.x = DIRECTION_UP.x;
        direction.y = DIRECTION_UP.y;
      } else if (g->y > minY) {
        /* to DOWN */
        direction.x = DIRECTION_DOWN.x;
        direction.y = DIRECTION_DOWN.y;
      }
    } else if (g->y == minY) {
      if (g->x < minX) {
        /* to RIGHT */
        direction.x = DIRECTION_RIGHT.x;
        direction.y = DIRECTION_RIGHT.y;
      } else if (g->x < minX) {
        /* to LEFT */
        direction.x = DIRECTION_LEFT.x;
        direction.y = DIRECTION_LEFT.y;
      }
    } else if (minX > g->x && minY > g->y) {
      /* to UP RIGHT */
      direction.x = DIRECTION_UP_RIGHT.x;
      direction.y = DIRECTION_UP_RIGHT.y;
    } else if (minX > g->x && minY < g->y) {
      /* to DOWN RIGHT */
      direction.x = DIRECTION_DOWN_RIGHT.x;
      direction.y = DIRECTION_DOWN_RIGHT.y;
    } else if (minX < g->x && minY > g->y) {
      /* to UP LEFT */
      direction.x = DIRECTION_UP_LEFT.x;
      direction.y = DIRECTION_UP_LEFT.y;
    } else if (minX < g->x && minY < g->y) {
      /* to DOWN LEFT */
      direction.x = DIRECTION_DOWN_LEFT.x;
      direction.y = DIRECTION_DOWN_LEFT.y;
    }
    
    /* Calc new position from currect position and direction move to. */
    point_t newPosition = {g->x + direction.x, g->y + direction.y};
    
    /* TODO: That big part! */

    if (!is_out_of_border_bounds(&newPosition)) {
      //process_new_position(&newPosition, gibies[i], GIBY_SYMB);
      /* Check two giby collision */
      if (is_cell_giby(board, &newPosition) || is_cell_shadok(board, &newPosition)) {
        /* Giby and shadok don't move */
      } else {
        move_player(board, &newPosition, gibies[i], GIBY_SYMB, flowers);
      }
    }

#ifdef _SHADOK_DEBUG_MESSAGES
    printf("[MIN]%d = %d\tminX = %d\tminY = %d\n", i, min, minX, minY);
#endif
  }
  
}
