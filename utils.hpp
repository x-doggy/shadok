#ifndef _SHADOK_UTILS_H
#define _SHADOK_UTILS_H

#include <cstdio>
#include <ctime>
#include <cassert>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include <cctype>

int rand_range(int min, int max);
void screen_clear();
int getline_correct(char *buf, size_t size);
void transform_to_lower_case(char d[]);
int hypot_squared(const int k1, const int k2);

#endif
