#include "utils.hpp"

int rand_range(int min, int max) {
  if (min == max) {
    return min;
  }
  if (min > max) {
    int tmp = min;
    min = max;
    max = tmp;
  }
  return min + (int)( (double)max * rand() / (RAND_MAX + 1.) );
}

void screen_clear() {
#if defined(_WIN32) || defined(_WIN64)
  system("@cls||clear");
#else
  printf("\033[2J\033[1;1H");
#endif
}

int getline_correct(char *buf, size_t size) {
  int i = 0;
  int ch;
  while ((ch = fgetc(stdin)) != EOF) {
    if (i + 1 < size) {
      buf[i] = (char) ch;
    }
    if (ch == '\n') {
      buf[i] = '\0';
      break;
    }
    ++i;
  }
  buf[i] = '\0';
  if (i == 0) { 
    return EOF;
  }
  return i;
}

void transform_to_lower_case(char d[]) {
  for (char *t = d; *t != '\0'; ++t) {
    *t = (char) tolower(*t);
  }
}

int hypot_squared(const int k1, const int k2) {
  return k1 * k1 + k2 * k2;
}
